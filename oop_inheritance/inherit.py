
class Animal:
    def __init__(self):
        print('animal init')
        self.nick_name = "animal nick name"
    def play(self):
        print(f'playing with {self.nick_name}')

class Dog(Animal):
    def __init__(self):
        print('dog created...')
        Animal.__init__(self)
    def walk_in_park(self):
        print(f'walking in park with {self.nick_name}')

rexi = Dog()
rexi.play()

